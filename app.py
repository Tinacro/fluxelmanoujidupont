from flask import Flask, render_template, request, redirect, flash, url_for
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from models import Flows, Users, create_tables, drop_tables
from forms import SignIn, LogIn, AddFlux
import os, hashlib, feedparser


app = Flask(__name__)
app.config['SESSION_TYPE'] = 'memcached'
app.config['SECRET_KEY'] = os.urandom(16).hex()

login_manager = LoginManager() 
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return Users.get(id=user_id)

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/signin', methods=['GET', 'POST', ])
def signin():

    form = SignIn()

    if form.validate_on_submit():
        password = form.password.data
        email = form.email.data
        password = hashlib.sha1(password.encode()).hexdigest()
        user = Users.select().where(Users.email == email).first()
        if(user == None):
            utilisateur = Users()
            utilisateur.password = password
            utilisateur.email = email
            utilisateur.save()
            return redirect(url_for('login'))
        else:
            return redirect(url_for('signin'))

    return render_template('signin.html', form=form)    

@app.route('/login', methods=['GET', 'POST', ])
def login():
    
    form = LogIn()

    if form.validate_on_submit():
        email = form.email.data
        password = hashlib.sha1(form.password.data.encode()).hexdigest()
        user = Users.select().where((Users.email == email) & (Users.password == password)).first()
        if (user != None):
            login_user(user)
            current_user.idUtilisateur = user.id
            return redirect(url_for('feed'))

    return render_template('login.html', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route('/feed')
@login_required
def feed():
    listeFluxParse = []
    listeFluxUser = Flows.select().where(Flows.userID == current_user.id)
    for item in listeFluxUser :
        fluxparse = feedparser.parse(item.flowURL) 
        listeFluxParse.append(fluxparse)
    return render_template('feed.html',listeFluxParse=listeFluxParse,listeFluxUser=listeFluxUser,zip=zip(listeFluxParse,listeFluxUser))

@app.route('/addFlux', methods=['GET', 'POST', ])
@login_required
def addFlux():
    form = AddFlux()
    if form.validate_on_submit():
        monfluxBDD = Flows.select().where((Flows.flowURL == form.lien.data) & (Flows.userID == current_user.id)).first()
        if(monfluxBDD == None):
            flow = Flows()
            flow.userID = current_user.id
            flow.flowURL = form.lien.data
            flow.save()
            return redirect(url_for('feed'))   
        else:
            return redirect(url_for('addFlux'))
    return render_template('addFlux.html',form=form)

@app.route('/supprimerFlux', methods=['GET','POST'])
@login_required
def supprimerFlux():
    idFluxUser = request.args.get('userID')
    linkFlux = request.args.get('flowURL')
    requete = Flows.delete().where((Flows.userID == idFluxUser) & (Flows.flowURL == linkFlux))
    requete.execute()
    return redirect(url_for('feed'))

@app.route('/flux', methods=['GET', 'POST'])
@login_required
def visionnerFlux():
    linkFlux = request.args.get('flowURL')
    fluxparse = feedparser.parse(linkFlux)
    element = feedparser.parse(linkFlux).entries
    return render_template('viewFlux.html',fluxparse=fluxparse, element=element)

@app.cli.command()
def initdb():
    create_tables()

@app.cli.command()
def dropdb():
    drop_tables()
