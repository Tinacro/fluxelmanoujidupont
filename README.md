Réalisé par Dupont Nathan et Marouane Elmanouji
## Projet Flux
Ce projet consiste a créer un système d'adhsion à differents flux et afficher,
selon chaque utilisateur.

## Structure
Le projet ce compose en deux templates: 
-l'un pour l'ecran d'acceuil et le système
d'identification et inscription
-l'autre sert à la gestion des flux
Apres etre inscris l'utilisateur va pouvoir observer la listes des flux auquels
il est deja inscris et peux si il le souhaite supprimer certaine.
Et sinon l'utilisateur peux effectuer un ajout de flux cia la barre de navigation
qui le redirigera vers une page de saisie.

## Installation
Avec le cmd dans le repertoire du projet effectuer:
'''
    $ pipenv install
'''

## Démarrage
'''
    $ pipenv shell 
    $ flask run 
'''